'use strict';

const loglevel = process.env.LOGLEVEL || 'trace'
const secretkey = 'encryption'

let dbconfig = {
    user: 'p6yJA5muF1',
    password: 'jyv1phUwC4',
    host: 'remotemysql.com',
    database: 'p6yJA5muF1',
    port: 3306
}

module.exports = {
    dbconfig,
    secretkey,
    webPort: process.env.PORT || 3010,
    logger: require('tracer')
        .console({
            format: [
                "{{timestamp}} <{{title}}> {{file}}:{{line}} : {{message}}"
            ],
            preprocess: function (data) {
                data.title = data.title.toUpperCase();
            },
            dateformat: "isoUtcDateTime",
            level: loglevel
        })

}