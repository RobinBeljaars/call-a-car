var mysql = require('mysql');
var config = require('./config.js');
let dbconfig = config.dbconfig
const logger = config.logger

const connectionSettings = {
    connectionLimit: 20,
    host: process.env.DB_HOST || dbconfig.host,
    user: process.env.DB_USER || dbconfig.user,
    password: dbconfig.password,
    database: process.env.DB_DATABASE || dbconfig.database,
    dateStrings: 'date',
    port: dbconfig.port,
    debug: false
}

var pool

pool = mysql.createPool(connectionSettings)

pool.on('acquire', (connection) => {
    // logger.trace('Connection %d acquired', connection.threadId)
})

pool.on('connection', (connection) => {
    // logger.trace('Connection to database was made')
})

pool.on('enqueue', () => {
    // logger.trace('Waiting for available connection slot')
})

pool.on('release', (connection) => {
    // logger.trace('Connection %d released', connection.threadId)
})

module.exports = pool
