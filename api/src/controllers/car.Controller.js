const assert = require('assert')
const RDWSearch = require('node-rdw')
const rdwSearch = new RDWSearch();

const car = require('../models/car.model.js')
const cartype = require('../models/cartype.js')
const gerage = require('../models/parking.model.js')
const ApiError = require('../models/ApiError.js')
const logger = require('../config/config').logger
const locHelper = require('../helpers/locationhelper');
const order = require('../models/order.js');

async function closest(req, res) {

    car.retrieveCars(req.body.cap, req.body.typeId, req.body.merk).then(async (results) => {
        let result = [];
        for (const row of results) {
            await locHelper.getcordsdistance(req.body.origin, row.locatieAddress).then(distance => {
                row.distance = distance
                result.push(row)
            }).catch(err => { throw err })
        }
        return result
    }).then(users => {
        var test = users.reduce((min, current) => {
            if (current.distance < min.distance) {
                return current
            } else if (current.distance >= min.distance) {

                return min
            } else {
                return current
            }
        })
        res.status(200).json(test)
    }).catch(err => { throw err })
}

function retrieveAllCars(req, res, next) {
    car.retrieveAllCars().then(result => {
        console.log(result)
        res.status(200).json(result)
    })
}

async function createCar(req, res, next) {
    const carBody = req.body
    try {
        assert(typeof (carBody.kenteken) === 'string', 'kenteken must be a string')
        assert(checkPlate(carBody.kenteken), 'kenteken is niet een valide (Nederlandse) kenteken')
        assert(typeof (carBody.merk) === 'string', 'merk must be a string')
        assert(typeof (carBody.maxPersonen) === `number`, 'maxPerson must be a number')
        assert(typeof (carBody.typeId) === `number`, 'typeId must be a number')
        assert(typeof (carBody.parkeergerageId) === `number`, 'parkeergerageId must be a number')
        assert(typeof (carBody.state) === `string`, 'state must be a string')

    } catch (ex) {
        next(new ApiError(ex.toString(), 422))
        return
    }

    cartype.checkId(carBody.typeId).then(type => {
        return gerage.checkId(carBody.parkeergerageId)
    }).then(parkeren => {
        return car.createCar(carBody.kenteken, carBody.merk, carBody.maxPersonen, carBody.typeId, carBody.parkeergerageId, carBody.state)
    }).then(result => {
        res.status(200).json({ "message": "Your car has been succesfully added to the Call-a-Car service" })
    }).catch(error => {
        logger.info('In catch: error = ' + error)
        next(new ApiError(error, 401))
    })
}

async function checkPlate(plate) {
    rdwSearch.searchPlate(plate, function (err, data) {
        if (err) {
            return false;
        } else {
            return true
        }
    })
}

function carArrived(req, res, next) {
    order.carPickUpReady(req.body.orderId).then(result => {
        res.status(200).send("waiting confirmed")
    }).catch(error => {
        logger.info('In catch: error = ' + error)
        next(new ApiError(error, 401))
    })
}

function customerArrived(req, res, next) {
    order.carWaitingDone(req.body.orderId).then(result => {
        res.status(200).send("confirmed state")
    }).catch(error => {
        logger.info('In catch: error = ' + error)
        next(new ApiError(error, 401))
    })
}

function canIGo(req, res, next) {
    order.canIGo(req.body.id).then(result=>{
        res.status(200).send("You can Go")
    }).catch(error=>{
        res.status(200).send("wait Longer")
    })
}
module.exports = {
    closest,
    createCar,
    retrieveAllCars,
    checkPlate,
    carArrived,
    customerArrived,
    canIGo
}