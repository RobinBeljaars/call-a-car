const assert = require('assert')

const carController = require('./car.Controller')
const car = require('../models/car.model.js')
const orders = require('../models/order.js')
const customers = require('../models/user.model.js')
const ApiError = require('../models/ApiError.js')
const logger = require('../config/config').logger
const locHelper = require('../helpers/locationhelper')

function order(req, res, next) {
    const body = req.body
    try {
        assert(typeof (body.kenteken) === 'string', 'kenteken must be a string')
        assert(carController.checkPlate(body.kenteken), 'kenteken is niet een valide (Nederlandse) kenteken')
        assert(typeof (body.customerId) === 'number', 'customerId must be a number')
        assert(typeof (body.origin) === 'string', 'origin must be a string')
    } catch (ex) {
        next(new ApiError(ex.toString(), 422))
    }

    locHelper.getcord(req.body.origin).then(origin => {
        return customers.confirmId(body.customerId)
    }).then(confirmed => {
        return car.checkId(body.kenteken)
    }).then(result => {
        // car gets reserved
        return car.reserveCar(body.kenteken)
    }).then(car => {
        // place order
        return orders.createOrder(body.customerId, body.kenteken, body.origin)
    }).then(order => {
        const end = {
            kenteken: body.kenteken,
            message: "Order succesfully created car is on its way"
        }
        res.status(200).json(end)
    }).catch(error => {
        logger.info('In catch: error = ' + error)
        next(new ApiError(error, 401))
    })
}

module.exports = {
    order
}