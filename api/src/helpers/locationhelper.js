const Nominatim = require('nominatim-geocoder')
const geocoder = new Nominatim()

function getcordsdistance(originraw, destinationraw) {
    return new Promise((resolve, reject) => {
        getcord(originraw).then(origin => {
            getcord(destinationraw).then(destination => {
                resolve(getDistanceFromLatLonInKm(origin.lat, origin.lon, destination.lat, destination.lon))
            }).catch(err => reject(err))
        }).catch(err => reject(err))
    })
}

function getcord(origin) {
    return new Promise((resolve, reject) => {
        geocoder.search({ q: origin }).then((response, err) => {
            if (err) {
                reject(err)
            } else {
                let coordinates = {}
                coordinates.lat = response[0].lat
                coordinates.lon = response[0].lon
                resolve(coordinates)
            }
        })
    })
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    const R = 6371; // Radius of the earth in km
    let dLat = deg2rad(lat2 - lat1);  // deg2rad below
    let dLon = deg2rad(lon2 - lon1);
    let a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

module.exports = {
    getcordsdistance,
    getcord
}