const db = require('../config/db.js')

module.exports = {
    checkId(id) {
        return new Promise(function (resolve, reject) {
            const query = `SELECT kenteken FROM cars WHERE kenteken=` + db.escape(id)
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else if (res.length > 0) {
                    resolve(res)
                } else {
                    reject('Given does not exist')
                }
            })
        })
    },
    retrieveAllCars() {
        return new Promise(function (resolve, reject) {
            const query = `SELECT cars.kenteken, cars.merk, cars.maxPersonen, type.name FROM cars INNER JOIN type ON cars.typeId=type.id WHERE cars.state ="parked"`
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    retrieveCars(cap, typeId, merk) {
        return new Promise(function (resolve, reject) {
            const query = `SELECT cars.kenteken, cars.merk, parkeren.locatieAddress FROM cars INNER JOIN parkeren ON cars.parkeergerageId=parkeren.id WHERE cars.state ="parked" AND cars.maxPersonen>=` + db.escape(cap) + ` AND cars.typeId =` + db.escape(typeId) + ` AND cars.merk = ` + db.escape(merk)
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    createCar(kenteken, merk, maxPersonen, typeId, parkeergerageId, state) {
        return new Promise(function (resolve, reject) {
            const query = `INSERT INTO cars(kenteken, merk, maxPersonen, typeId, parkeergerageId, state) VALUES (` + db.escape(kenteken) + `,` + db.escape(merk) + `,` + db.escape(maxPersonen) + `,` + db.escape(typeId) + `,` + db.escape(parkeergerageId) + `,` + db.escape(state) + `)`
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    reserveCar(kenteken) {
        return new Promise(function (resolve, reject) {
            const query = `UPDATE cars SET state='driving' WHERE kenteken=` + db.escape(kenteken)
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    }
}

/** {
"kenteken": ,
"merk": ,
"maxPersonen": ,
"typeId": ,
"parkeergerageId": ,
"state": ,
"id": ,
"naam": ,
"locatieAddress": ,
"parkeerplekken": ,
"oplaadplekken": ,
"distance":
}  **/
