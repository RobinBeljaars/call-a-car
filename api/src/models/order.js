const { logger } = require('../config/config.js')
const db = require('../config/db.js')
var pad = function (num) { return ('00' + num).slice(-2) };

function closeUnsolved() {
    const query = `SELECT id FROM Orders WHERE dueDate<now() AND state='opened'`
    db.query(query, (err, res) => {
        if (err) {
            logger.info(err)
        } else {
            res.forEach(element => {
                console.log(element.id)
                const query = `UPDATE Orders SET state='canceled by car' WHERE id=` + element.id
                db.query(query, (err, res) => {
                    if (err) {
                        logger.info(err)
                    }
                })
            })
        }
    })
}

setInterval(closeUnsolved, 10000);

function addMinToDate(dt, minutes) {
    return new Date(dt + minutes * 60000);
}

module.exports = {
    createOrder(customerId, carId, origin) {
        return new Promise(function (resolve, reject) {
            const query = `INSERT INTO Orders(customerId, carId, state, origin) VALUES (` + db.escape(customerId) + `,` + db.escape(carId) + `, 'opened' ,` + db.escape(origin) + `)`
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    closeOrder(id) {
        return new Promise(function (resolve, reject) {
            const query = `UPDATE Orders SET state='canceled' WHERE id =` + db.escape(id)
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    carPickUpReady(id) {
        return new Promise(function (resolve, reject) {

            var date;
            date = addMinToDate(Date.now(), 10);
            date = date.getUTCFullYear() + '-' +
                pad(date.getUTCMonth() + 1) + '-' +
                pad(date.getUTCDate()) + ' ' +
                pad(date.getUTCHours()) + ':' +
                pad(date.getUTCMinutes()) + ':' +
                pad(date.getUTCSeconds());
            const query = `UPDATE Orders SET dueDate='` + date + `' WHERE id =` + db.escape(id)
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    carWaitingDone(id) {
        return new Promise(function (resolve, reject) {
            const query = `UPDATE Orders SET state='withCustomer' WHERE id =` + db.escape(id)
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    canIGo(id) {
        return new Promise(function (resolve, reject) {
            const query = `SELECT id FROM Orders WHERE id=` + db.escape(id) + ` AND (state='canceled' OR state='closed' OR state ='canceled by car')`
            db.query(query, (err, res) => {
                if (err) {
                    reject(err)
                } else if(res.length>0){
                    resolve(res)
                } else{
                    reject("empty list")
                }
            })
        })

    }
}