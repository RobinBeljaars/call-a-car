const express = require('express');

const router = express.Router({ mergeParams: true });
const carController = require('../controllers/car.Controller.js');

/** 
    POST api/car body contains adress ,arrival time ,amount of people and type (andere entiteit)
    POST api/car/fromhome body amount of people and type
    PUT api/car/{id} accept an request for a car
    POST api/car/requestPayment

 **/
router.get('/allCars', carController.retrieveAllCars)
router.post('/closestCar', carController.closest)
router.post('/supplyCar', carController.createCar)


router.post('/WaitingForCustomer', carController.carArrived)
router.post('/CustomerArrived', carController.customerArrived)


router.post('/CanIGo', carController.canIGo)

module.exports = router
