const express = require('express');

const router = express.Router({ mergeParams: true });
const orderController = require('../controllers/order.Controller.js');

router.post('/orderCar', orderController.order)


module.exports = router
