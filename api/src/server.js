const express = require('express')
const app = express()
const logger = require('./config/config.js').logger

const bodyparser = require('body-parser')

// const routes
const carRoutes = require('./routes/car.routes')
const orderRoutes = require('./routes/order.routes')

app.use(bodyparser.json())
 app.use(function (req, res, next) {
      res.setHeader('Access-Control-Allow-Origin', '*')
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
      res.setHeader('Access-Control-Allow-Headers', '*')
    //   res.setHeader('Access-Control-Allow-Credentials', true)
     next()
})

app.use('/api/cars', carRoutes)
app.use('/api/order', orderRoutes)

app.use((err, req, res, next) => {
    logger.error(err)
    res.status((err.code || 404)).json(err).end()
})



// app.use('*', (req, res) => {
//     res.json({
//         error: {
//             'name': 'Error',
//             'status': 404,
//             'message': 'Invalid Request',
//             'statusCode': 404
//         }
//     })
// })

module.exports = app;