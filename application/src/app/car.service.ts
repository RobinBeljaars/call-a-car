import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Car } from './car/car';
import { CAR } from './car/mock-cars';
import { Observable, of, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  requestCarUrl = 'https://callacarapi.herokuapp.com/api/cars/closestCar';
  //requestCarUrl = 'http://localhost:3000/api/cars/closestCar';

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  requestMockCar(): Observable<Car> {
    const car = of(CAR);
    return car;
  }

  requestCar(cap: number, type: number, origin: string, brand: string): Observable<Car> {
    var postBody = "{ \"cap\": " + cap + ", " +
                   " \"typeId\": " + type + ", " +
                   " \"origin\": \"" + origin + "\", " +
                   " \"merk\": \"" + brand + "\" }" ;
    console.log("Postbody: " + postBody)  
    
    return this.http.post<Car>(this.requestCarUrl, postBody, this.httpOptions)
    .pipe(
      map((response: any) => {
        const responseCar = {
          licensePlate: response.kenteken,
          brand: brand,
          comingFrom: response.locatieAddress,
          distance: response.distance,
          type: type
        }
        return responseCar
      }),
      catchError(this.handleError)
    );        
    //const car = of(CAR);
    //return car;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
 }
  
}
