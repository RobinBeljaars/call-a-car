import { Component, OnInit } from '@angular/core';
import { Car } from './car'
import { CAR } from './mock-cars'
import { CarService } from '../car.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  car: Car;


  constructor(private carService: CarService) { }

  requestCar(): void {
    this.carService.requestCar(2, 1, "Frans Lebretlaan 65 Dordrecht", "opal")
      .subscribe(car => this.car = car);
  }

  ngOnInit() {
    this.requestCar();
  }

}
