export interface Car {
    licensePlate: string;
    brand: string;
    comingFrom: string;
    distance: number;
    type: number;
}