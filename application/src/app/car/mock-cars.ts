import { Car } from './car';

export const CAR: Car = {
    licensePlate: 'Test-123',
    brand: 'Generic brand',
    comingFrom: 'Example street',
    distance: 3.23,
    type: 1
  };